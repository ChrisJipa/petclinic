import connection.HibernateUtils;
import model.*;
import repository.*;
import java.util.*;
public class Main {

    public static void main(String[] args) {

        VeterinarianRepository veterinarianRepository = new VeterinarianRepository();
        Veterinarian vet1 = new Veterinarian("John", "Doe");
        veterinarianRepository.createVeterinarian(vet1);

        Pet pet1 = new Pet("Bozo", "Dino");
        PetRepository petRep = new PetRepository();
        petRep.addPet(pet1);

        Consult consult1 = new Consult(new Date(50, 12, 12), "consult for dino");
        consult1.setPet(pet1);
        consult1.setVeterinarian(vet1);
        ConsultRepository consRep = new ConsultRepository();
        consRep.addConsult(consult1);

//        List<Veterinarian> vetsBeforeDelete = veterinarianRepository.getAllVets();
//        System.out.println("--------------Before delete-------------");
//        for(Veterinarian vet : vetsBeforeDelete) {
//            System.out.println(vet);
//        }

        // ============= 1 FIND ================
//        Veterinarian vetFoundById = veterinarianRepository.getVetById(1);
//        System.out.println("Vet found by id: " + vetFoundById);

        // ============= 1 DELETE ================
//        veterinarianRepository.deleteVet(vet1);
//        List<Veterinarian> vetsAfterDelete = veterinarianRepository.getAllVets();
//        System.out.println("--------------After delete-------------");
//        for(Veterinarian vet1 : vetsAfterDelete) {
//            System.out.println(vet1);
//        }
        // ============= 1 UPDATE ================
//        vet1.setAddress("Middle of nowhere");
//        vet1.setSpeciality(Speciality.CATS);
//        veterinarianRepository.updateVet(vet1);

        // ============= 3 UPDATE ================


        HibernateUtils.shutdown();
    }
}
