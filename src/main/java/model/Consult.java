package model;

import java.sql.*;
import javax.persistence.*;
import java.util.*;
import java.util.Date;

@Entity
@Table(schema = "petclinic", name = "consult")
public class Consult {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "vet_id", foreignKey = @ForeignKey(name = "fk_veterinarian_consult"))
    private Veterinarian veterinarian;

    @ManyToOne
    @JoinColumn(name = "pet_id", foreignKey = @ForeignKey(name = "fk_pet_consult"))
    private Pet pet;

    @Column
    @Temporal(TemporalType.DATE)
    private Date date;

    @Column
    private String description;

    //Constructors
    public Consult(Date date, String description) {
        this.setDate(date);
        this.setDescription(description);
    }

    public Consult() {
    }

    //Setters
    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public void setVeterinarian(Veterinarian veterinarian) {
        this.veterinarian = veterinarian;
    }

    //Getters

    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Consult consult = (Consult) o;
        return id.equals(consult.id) && date.equals(consult.date) && description.equals(consult.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, description);
    }
}
