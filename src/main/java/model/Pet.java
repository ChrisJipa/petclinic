package model;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(schema = "petclinic", name = "pet")
public class Pet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String name;

    @Column
    private String race;

    @Column
    @Temporal(TemporalType.DATE)
    private Date birthdate;

    @Column
    private boolean isVaccinated;

    @Column
    private String ownerName;

    @OneToMany(mappedBy = "pet")
    private List<Consult> consults = new ArrayList<Consult>();

    //Constructors
    public Pet(String name, String race) {
        this.setName(name);
        this.setRace(race);
    }

    public Pet() {
    }

    //Setters
    public void setName(String name) {
        this.name = name;
    }

    public void setRace(String race) {
        this.race = race;
    }
}
