package model;

public enum Speciality {
    DOGS,
    CATS,
    OTHERS
}
