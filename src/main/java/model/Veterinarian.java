package model;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(schema = "petclinic", name = "veterinarian")
public class Veterinarian {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    private String address;

    @Enumerated(value = EnumType.STRING)
    private Speciality speciality;

    @OneToMany(mappedBy = "veterinarian")
    private List<Consult> consults = new ArrayList<Consult>();

    //Constructors
    public Veterinarian(){}

    public Veterinarian(String firstName, String lastName){
        this.setFirstName(firstName);
        this.setLastName(lastName);
    }

    //Setters
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setSpeciality(Speciality speciality) {
        this.speciality = speciality;
    }

    //Getters
    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    @Override
    public String toString() {
        return "Veterinarian" +
                "id = " + id +
                ", Name = '" + firstName + '\'' + lastName + '\'' +
                ", address ='" + address + '\'' +
                ", speciality ='" + speciality + '\'';
    }

}
