package repository;

import model.Consult;
import java.util.*;

import connection.HibernateUtils;
import org.hibernate.*;

public class ConsultRepository {

    public List<Consult> getAllConsults() {
        Session session = HibernateUtils.getSessionFactory().openSession();
        List<Consult> consults = session.createQuery("from Consult", Consult.class).list();
        session.close();
        return consults;
    }

    public Consult getById(Integer id) {
        Consult consult = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            consult = session.find(Consult.class, id);
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return consult;
    }

    public void addConsult(Consult consult) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.save(consult);
            transaction.commit();
            session.close();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public void updateConsult(Consult consult) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.update(consult);
            transaction.commit();
            session.close();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }


    public void updateConsultDescription(Integer consultId, String newDescription) {
        Transaction transaction = null;
        Consult consult = getById(consultId);
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            consult.setDescription(newDescription);
            session.update(consult);
            transaction.commit();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
