package repository;

import model.Veterinarian;
import connection.HibernateUtils;
import org.hibernate.*;

import java.util.*;

public class VeterinarianRepository {

    public void createVeterinarian(Veterinarian veterinarian) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.save(veterinarian);
            transaction.commit();
            session.close();
            System.out.println("New vet created!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Veterinarian getVetById(int id) {
        Veterinarian vet = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            vet = session.find(Veterinarian.class, id);
            session.close();
        }catch (Exception e) {
            e.printStackTrace();
        }
        return vet;
    }

    public List<Veterinarian> getAllVets() {
        Session session = HibernateUtils.getSessionFactory().openSession();
        List<Veterinarian> vets = session.createQuery("from Veterinarian", Veterinarian.class).list();
        session.close();
        return vets;
    }

    public void deleteVet(Veterinarian vet) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.delete(vet);
            transaction.commit();
            System.out.println("Delete successful!");
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public void updateVet(Veterinarian vet) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.update(vet);
            transaction.commit();
            System.out.println("Update successful!");
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

}
