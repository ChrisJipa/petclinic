import model.Pet;
import org.junit.Test;
import repository.PetRepository;


import static org.junit.Assert.*;

import java.util.*;

public class PetRepositoryTest {

    @Test
    public void addPetTest() {
        //Given
        Pet pet = new Pet("Fifi", "T-Rex");
        PetRepository petRep = new PetRepository();
        //When
        petRep.addPet(pet);
        //Then
        List<Pet> results = petRep.getAllPets();
        assertTrue(results.size() > 0);
        //Cleanup
        petRep.deletePet(pet);
    }

    @Test
    public void deletePetTest() {
        //Given
        Pet pet = new Pet("Bozo", "Dog");
        PetRepository petRep = new PetRepository();
        petRep.addPet(pet);
        //When
        petRep.deletePet(pet);
        //Then
        List<Pet> results = petRep.getAllPets();
        assertFalse(results.contains(pet));
    }
}
