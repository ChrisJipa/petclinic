import model.Consult;
import org.junit.Test;
import repository.ConsultRepository;
import org.junit.Test.*;

import static org.junit.Assert.*;

import java.util.*;

public class ConsultRepositoryTest {

    @Test
    public void givenConsultWhenAddingNewConsultThenSaveConsult() {
        ConsultRepository consRep = new ConsultRepository();
        Consult consult = new Consult(new Date(2020, 12, 12), "Consult for a dinosaur");
        consRep.addConsult(consult);
        List<Consult> consultList = consRep.getAllConsults();
        assertTrue(consultList.contains(consult));
    }

    @Test
    public void givenConsultWhenUpdatingConsultThenConsultIsUpdated() {
        //Setup
        ConsultRepository consRep = new ConsultRepository();
        Consult consult = new Consult(new Date(2020, 12, 12), "Consult for a dinosaur");
        consRep.addConsult(consult);
        consult.setDate(new Date(2020, 12, 01));
        //Execute
        consRep.updateConsult(consult);
        //Verify
        Consult testConsult = consRep.getById(1);
        assertEquals(testConsult, consult);
    }

    @Test
    public void givenNewDescriptionConsultWhenUpdatingConsultWithNewDescriptionThenConsultIsUpdated() {
        //Setup
        ConsultRepository consRep = new ConsultRepository();
        Consult consult = new Consult(new Date(2020, 12, 12), "Consult for a dinosaur");
        consRep.addConsult(consult);
        //Execute
        consRep.updateConsultDescription(1,"Consult for a triceratops");
        //Verify
        Consult testConsult = consRep.getById(1);
        String description = testConsult.getDescription();
        assertEquals("Consult for a triceratops", description);
    }


}
