import model.Veterinarian;
import org.junit.Test;
import repository.VeterinarianRepository;
import java.util.*;
import static org.junit.Assert.*;

public class VeterinarianRepositoryTest {

    @Test
    public void createVetTest() {
        //Given
        Veterinarian vet = new Veterinarian("John", "Doe");
        VeterinarianRepository vetRepository = new VeterinarianRepository();
        //When
        vetRepository.createVeterinarian(vet);
        //Then
        List <Veterinarian> results = vetRepository.getAllVets();
        assertTrue(results.size() > 0);
    }

    @Test
    public void deleteVet() {
        //Given
        Veterinarian vet = new Veterinarian("Clark", "Kent");
        VeterinarianRepository vetRepository = new VeterinarianRepository();
        vetRepository.createVeterinarian(vet);
        //When
        vetRepository.deleteVet(vet);
        //Then
        List <Veterinarian> results = vetRepository.getAllVets();
        assertFalse(results.contains(vet));

    }

}
